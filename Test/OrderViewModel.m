//
//  OrderViewModel.m
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderViewModel.h"
#import "CreateViewModel.h"
#import "ViewModelServices.h"
#import "OrderImpl.h"
#import "User.h"

@interface OrderViewModel ()

@property (nonatomic, weak) id<ViewModelServices> services;

@end

@implementation OrderViewModel

#pragma mark - init methods

-(instancetype) initWithServices:(id)services
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.services = services;
    [self intialized];
    return self;
}

-(void) intialized
{
    self.exitCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
       return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
           [self.services logout];
           [subscriber sendCompleted];
           return nil;
       }];
    }];
    
    self.addCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
       return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
           CreateViewModel *viewModel = [[CreateViewModel alloc] initWithServices:self.services];
           [self.services pushViewModel:viewModel animated:YES];
           [subscriber sendCompleted];
           return nil;
       }];
    }];
    
}

-(NSString *) changeStatusTo:(NSString *)status forID:(NSInteger)orderID
{
    NSString *st = status;
    OrderImpl *orderImpl = [[self.services getRealmServices] getObjectFromID:orderID forClass:[OrderImpl class]];
    User *user = [[self.services getRealmServices] getAllObjectForClass:[User class]].firstObject;
    BOOL agreedAdmin1 = orderImpl.agreedAdmin1;
    BOOL agreedAdmin2 = orderImpl.agreedAdmin2;
    
    
    if ([status isEqualToString:@"Отказ"])
    {
        st = @"Отказ";
    }
    if ([status isEqualToString:@"Согласовано"])
    {
       st = @"На расмотрений";
       
        if ([user.username isEqualToString:@"adm2"])
        {
            agreedAdmin1 = YES;
        }
        if ([user.username isEqualToString:@"adm3"])
        {
            agreedAdmin2 = YES;
        }
        
       if (agreedAdmin1 == YES && agreedAdmin2 == YES)
       {
           st = @"Согласовано";
       }
        
    }
    if ([status isEqualToString:@"На расмотрений"])
    {
        st = @"На расмотрений";
    }
    [[self.services getRealmServices] updateObjectWithBlock:^{
        
        if ([st isEqualToString:@"Отказ"])
        {
            orderImpl.agreedAdmin1 = NO;
            orderImpl.agreedAdmin2 = NO;
        } else
        {
            orderImpl.agreedAdmin1 = agreedAdmin1;
            orderImpl.agreedAdmin2 = agreedAdmin2;
        }
        
        orderImpl.status = st;
    }];
    
    return st;
}

#pragma mark - Publick API methods

-(NSArray *)getOrders
{
  return [[self.services getRealmServices] getAllObjectForClass:[OrderImpl class]];
}

-(BOOL) isRoot
{
    User *user = [[self.services getRealmServices] getAllObjectForClass:[User class]].firstObject;
    return [user rooted];
}

@end