//
//  OrderViewController.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderViewController : UIViewController

-(instancetype) initWithViewModel:(id)viewModel;

@end