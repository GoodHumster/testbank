//
//  OrderImpl.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Realm/Realm.h>
#import "Order.h"

@interface OrderImpl : RLMObject<Order>
@end
