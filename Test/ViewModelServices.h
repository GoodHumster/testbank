//
//  ViewModelServices.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RealmServices.h"

@protocol ViewModelServices <NSObject>

-(void) pushViewModel:(id)viewModel animated:(BOOL)animated;
-(void) popAnimated:(BOOL)animated;

-(id<RealmServices>) getRealmServices;

-(void) saveCookie;
-(void) logout;

@end