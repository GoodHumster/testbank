//
//  ViewModelServicesImpl.m
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ViewModelServicesImpl.h"
#import "LoginViewModel.h"
#import "LoginViewController.h"
#import "RealmServicesImpl.h"
#import "OrderViewModel.h"
#import "OrderViewController.h"
#import "CreateViewModel.h"
#import "CreateViewController.h"
#import "User.h"

NSString *const kSessionKey = @"basicSession";

@interface ViewModelServicesImpl ()

@property (nonatomic, retain) UINavigationController *navigationController;

@property (nonatomic, strong) RealmServicesImpl *realmServices;

@end

@implementation ViewModelServicesImpl

#pragma mark - init methods

@synthesize navigationController = _navigationController;

-(instancetype) initWithNavigationController:(id)navigation
{
    if  ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.navigationController = navigation;
    self.realmServices = [[RealmServicesImpl alloc] init];
    [self switchToAutorizationIfNeeded];
    return self;
}

#pragma mark - Publick API methods

-(void) pushViewModel:(id)viewModel animated:(BOOL)animated
{
    UIViewController *vc = nil;
    if ([viewModel isKindOfClass:[LoginViewModel class]])
    {
        vc = [[LoginViewController alloc] initWithViewModel:viewModel];
    }
    if ([viewModel isKindOfClass:[OrderViewModel class]])
    {
        vc = [[OrderViewController alloc] initWithViewModel:viewModel];
    }
    if ([viewModel isKindOfClass:[CreateViewModel class]])
    {
        vc = [[CreateViewController alloc] initWithViewModel:viewModel];
    }
    if (vc == nil)
    {
        return;
    }
    [self pushViewController:vc animated:animated];
}

-(void) popAnimated:(BOOL)animated
{
    [self.navigationController popViewControllerAnimated:animated];
}

-(id<RealmServices>) getRealmServices
{
    return self.realmServices;
}

-(void) saveCookie
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"Login" forKey:kSessionKey];
}

-(void) logout
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:kSessionKey];
    [self.realmServices removeObjectForClass:[User class]];
    [self switchToAutorizationIfNeeded];
}

#pragma mark - uitilit methods

-(void) pushViewController:(UIViewController *)controller animated:(BOOL)animated
{
    self.navigationController.delegate = nil;
    [self.navigationController pushViewController:controller animated:animated];
}

-(void) switchToAutorizationIfNeeded
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:kSessionKey])
    {
        OrderViewModel *viewModel = [[OrderViewModel alloc] initWithServices:self];
        [self pushViewModel:viewModel animated:YES];
        return;
    }
    
    LoginViewModel *viewModel = [[LoginViewModel alloc] initWithServices:self];
    [self pushViewModel:viewModel animated:YES];
}

@end