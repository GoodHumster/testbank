//
//  OrderTableViewCellDelegate.h
//  Test
//
//  Created by ios on 03.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OrderTableViewCellDelegate <NSObject>

-(NSString *) changeStatusTo:(NSString *)status forID:(NSInteger)orderID;

@end
