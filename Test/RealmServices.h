//
//  RealmServices.h
//  objc.mss
//
//  Created by ios on 21.04.16.
//  Copyright © 2016 MSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RealmServices <NSObject>

-(void) removeAllDatabase;

-(void) saveObject:(id)object;

-(NSArray *)getAllObjectForClass:(Class)cls;

-(id) getObjectFromID:(NSInteger)id forClass:(Class)cls;

-(void) updateObjectWithBlock:(void(^)(void))block;

-(NSDate *) lastDatabaseWriteDate;

-(void) removeObjectForClass:(Class)cls;

@end