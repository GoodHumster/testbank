//
//  CreateViewModel.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>


@interface CreateViewModel : NSObject

-(instancetype) initWithServices:(id)services;

@property (nonatomic, strong) RACCommand *addCommand;
@property (nonatomic, strong) RACCommand *backCommand;

-(void) enterUsername:(NSString *)text;
-(void) enterDescription:(NSString *)text;


@end