//
//  LoginViewModel.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface LoginViewModel : NSObject

-(instancetype) initWithServices:(id)services;

@property (nonatomic, strong) RACCommand *loginCommand;

-(void) enterUsername:(NSString *)text;

-(void) enterPassword:(NSString *)text;

@end
