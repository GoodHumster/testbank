//
//  OrderTableViewCell.m
//  Test
//
//  Created by ios on 03.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderTableViewCell.h"

@interface OrderTableViewCell ()<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@property (weak, nonatomic) IBOutlet UIButton *changeStatusButton;

@property (nonatomic) NSInteger orderId;

@end


@implementation OrderTableViewCell

-(void) awakeFromNib
{
    [super awakeFromNib];
    self.changeStatusButton.layer.cornerRadius = 4;
    self.changeStatusButton.layer.borderWidth = 1.0;
    self.changeStatusButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void) configWithUsername:(NSString *)usr description:(NSString *)dsc andStatus:(NSString *)status isRooted:(BOOL)rooted andOrderID:(NSInteger)id
{
    self.usernameLabel.text = usr;
    self.descriptionLabel.text = dsc;
    [self.changeStatusButton setTitle:status forState:UIControlStateNormal];
    self.orderId = id;
    self.changeStatusButton.userInteractionEnabled = rooted;
}


- (IBAction)changeStatus:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Выберите статус" message:nil delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"На расмотрений",@"Отказ",@"Согласовано", nil];
    [alertView show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
  NSString *status = [self.delegate changeStatusTo:title forID:self.orderId];
  [self.changeStatusButton setTitle:status forState:UIControlStateNormal];
}

@end