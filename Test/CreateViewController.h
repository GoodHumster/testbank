//
//  CreateViewController.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateViewController : UIViewController

-(instancetype) initWithViewModel:(id)viewModel;

@end