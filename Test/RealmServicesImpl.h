//
//  RealmServicesImpl.h
//  objc.mss
//
//  Created by ios on 21.04.16.
//  Copyright © 2016 MSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RealmServices.h"

@interface RealmServicesImpl : NSObject<RealmServices>
@end