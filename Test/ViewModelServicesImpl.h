//
//  ViewModelServicesImpl.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServices.h"

@interface ViewModelServicesImpl : NSObject<ViewModelServices>

-(instancetype) initWithNavigationController:(id) navigation;

@end