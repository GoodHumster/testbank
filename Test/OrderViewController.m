//
//  OrderViewController.m
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderViewController.h"
#import "OrderViewModel.h"
#import "OrderTableViewCell.h"
#import "OrderTableViewCellDelegate.h"
#import "Order.h"

@interface OrderViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) OrderViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UIButton *exit;
@property (weak, nonatomic) IBOutlet UIButton *create;

@property (nonatomic, strong) NSArray *source;

@end

@implementation OrderViewController

#pragma mark - init methods

-(instancetype) initWithViewModel:(id)viewModel
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    self.source = [NSArray new];
    return self;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self setupDefaults];
    [self bindViewModel];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.source = [self.viewModel getOrders];
    [self.tableView reloadData];
}

-(void) bindViewModel
{
    self.exit.rac_command = self.viewModel.exitCommand;
    self.create.rac_command = self.viewModel.addCommand;
}



-(void) setupDefaults
{
    self.navigationView.layer.masksToBounds = NO;
    self.navigationView.layer.shadowOffset = CGSizeMake(0, 5);
    self.navigationView.layer.shadowOpacity = 1.0;
    self.navigationView.layer.shadowRadius = 3;
    self.navigationView.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.26].CGColor;
    
    self.create.hidden = [self.viewModel isRoot];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"cellID"];
    self.tableView.estimatedRowHeight = 300;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

}

#pragma mark - tableview datasource

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.source.count;
}

#pragma mark - tableview delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    id<Order> order = self.source[indexPath.row];
    [cell configWithUsername:[order username] description:[order problem] andStatus:[order status] isRooted:[self.viewModel isRoot] andOrderID:[order id]];
    cell.delegate = self.viewModel;
    return cell;
}

@end