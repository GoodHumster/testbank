//
//  LoginViewController.m
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginViewController.h"
#import "LoginViewModel.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *userText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;


@property (nonatomic, strong) LoginViewModel *viewModel;

@end

@implementation LoginViewController

#pragma mark - init methods

-(instancetype) initWithViewModel:(id)viewModel
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    return self;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self setupDefaults];
    [self bindViewModels];
}

-(void) bindViewModels
{
   [self.userText.rac_textSignal subscribeNext:^(id x) {
        [self.viewModel enterUsername:x];
    }];
    
    [self.passwordText.rac_textSignal subscribeNext:^(id x) {
        [self.viewModel enterPassword:x];
    }];
    
    self.loginButton.rac_command = self.viewModel.loginCommand;
    
    [self.loginButton.rac_command.enabled subscribeNext:^(id value) {
        if (![value boolValue])
        {
            self.loginButton.backgroundColor = [UIColor clearColor];
            return;
        }
        self.loginButton.backgroundColor = [UIColor colorWithRed:59.0/255.0 green:64.0/255.0 blue:84.0/255.0 alpha:1.0];
    }];
}

-(void) setupDefaults
{
    UIView *paddingLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 14)];
    
    self.userText.leftView = paddingLeft;
    self.userText.leftViewMode = UITextFieldViewModeAlways;
    
    paddingLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 14)];
    self.passwordText.leftView = paddingLeft;
    self.passwordText.leftViewMode = UITextFieldViewModeAlways;
    
    self.loginButton.layer.cornerRadius = 4;
    self.loginButton.layer.borderWidth = 1.5;
    self.loginButton.layer.borderColor = [UIColor colorWithRed:59.0/255.0 green:64.0/255.0 blue:84.0/255.0 alpha:1.0].CGColor;
}

#pragma mark - action

- (IBAction)login:(id)sender {
}

@end