//
//  LoginViewModel.m
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginViewModel.h"
#import "ViewModelServices.h"
#import "OrderViewModel.h"
#import "User.h"


@interface LoginViewModel ()

@property (nonatomic, weak) id<ViewModelServices> services;

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;

@property (nonatomic, strong) NSDictionary *users;

@end

@implementation LoginViewModel

-(instancetype) initWithServices:(id)services
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    
    self.users = @{@"user1":@"111",@"adm2":@"222",@"adm3":@"333"};
    
    self.services = services;
    [self initialized];
    return self;
}

-(void) initialized
{
    RACSignal *enabledSignal = [[RACSignal combineLatest:@[RACObserve(self, username),RACObserve(self, password)] reduce:^id{
      
        return [NSNumber numberWithBool:self.username.length > 0 && self.password.length > 0];
    }] map:^id(id value) {
        return @([value boolValue]);
    }];
    
    self.loginCommand = [[RACCommand alloc] initWithEnabled:enabledSignal signalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            
            NSString *passwrod = self.users[self.username];
            
            if (passwrod == nil || passwrod.length == 0)
            {
                [subscriber sendCompleted];
                return nil;
            }
            if ([passwrod isEqualToString:self.password])
            {
                [self.services saveCookie];
                OrderViewModel *viewModel = [[OrderViewModel alloc] initWithServices:self.services];
                User *user = [[User alloc] init];
                user.username = self.username;
                user.rooted = [self.username  isEqual: @"adm2"] || [self.username  isEqual: @"adm3"];
                [[self.services getRealmServices] saveObject:user];
                [self.services pushViewModel:viewModel animated:YES];
            }
            
            [subscriber sendCompleted];
            return nil;
        }];
        
    }];
}

#pragma mark - Publick API methods

-(void) enterPassword:(NSString *)text
{
    self.password = text;
}

-(void) enterUsername:(NSString *)text
{
    self.username = text;
}

@end