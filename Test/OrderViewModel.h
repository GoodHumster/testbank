//
//  OrderViewModel.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "OrderTableViewCellDelegate.h"

@interface OrderViewModel : NSObject<OrderTableViewCellDelegate>

-(instancetype) initWithServices:(id)services;

@property (nonatomic, strong) RACCommand *exitCommand;
@property (nonatomic, strong) RACCommand *addCommand;

-(NSArray *) getOrders;

-(BOOL) isRoot;

@end