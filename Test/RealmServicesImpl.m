//
//  RealmServiceImpl.m
//  objc.mss
//
//  Created by ios on 21.04.16.
//  Copyright © 2016 MSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RealmServicesImpl.h"
#import <Realm/Realm.h>
#import <Realm/RLMRealm_Dynamic.h>

#define schemeVersion 12

@implementation RealmServicesImpl

#pragma mark - Public API methods


-(void) removeAllDatabase
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
}

-(void) removeObjectForClass:(Class)cls
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    RLMResults *results = [realm allObjects:NSStringFromClass(cls)];
    [realm deleteObjects:results];
    [realm commitWriteTransaction];
}

-(void) saveObject:(id)object
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:object];
    [realm commitWriteTransaction];
}


-(NSArray *)getAllObjectForClass:(Class)cls
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *result = [realm allObjects:NSStringFromClass(cls)];
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < result.count; i++)
    {
        [resultArray addObject:[result objectAtIndex:i]];
    }
    return resultArray;
}

-(void) updateObjectWithBlock:(void (^)(void))block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    if (block != nil)
    {
        block();
    }
    [realm commitWriteTransaction];
}

-(id) getObjectFromID:(NSInteger)id forClass:(__unsafe_unretained Class)cls
{
    NSArray *results = [self getAllObjectForClass:cls];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@" self.id == %d",id];
    return [results filteredArrayUsingPredicate:predicate].firstObject;
}

+(BOOL) needMigration
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if (![userDefaults objectForKey:@"realmConfiguration"])
    {
        [userDefaults setObject:[NSNumber numberWithInt:schemeVersion] forKey:@"realmConfiguration"];
        return YES;
    }
    
    NSInteger realmSchemaVersion = [[userDefaults objectForKey:@"realmConfiguration"] integerValue];
    
    if (realmSchemaVersion < schemeVersion)
    {
        [userDefaults setObject:[NSNumber numberWithInt:schemeVersion] forKey:@"realmConfiguration"];
        return YES;
    }
    RLMRealmConfiguration *realmConfig = [RLMRealmConfiguration defaultConfiguration];
    realmConfig.schemaVersion = schemeVersion;
    [RLMRealmConfiguration setDefaultConfiguration:realmConfig];
    return NO;
}



@end