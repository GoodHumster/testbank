//
//  OrderTableViewCell.h
//  Test
//
//  Created by ios on 03.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "OrderTableViewCellDelegate.h"
#import <UIKit/UIKit.h>

@interface OrderTableViewCell : UITableViewCell

-(void) configWithUsername:(NSString *)usr description:(NSString *)dsc andStatus:(NSString *)status isRooted:(BOOL)rooted andOrderID:(NSInteger)id;

@property (nonatomic, weak) id<OrderTableViewCellDelegate> delegate;

@end