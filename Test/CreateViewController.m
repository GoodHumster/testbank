//
//  CreateViewController.m
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CreateViewController.h"
#import "CreateViewModel.h"

@interface CreateViewController ()

@property (nonatomic, strong) CreateViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UITextView *problemText;
@property (weak, nonatomic) IBOutlet UITextField *usernameText;

@property (weak, nonatomic) IBOutlet UIButton *addPhotoBtn;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *back;

@end

@implementation CreateViewController

#pragma mark - init methods

-(instancetype) initWithViewModel:(id)viewModel
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    return self;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self setupDefaults];
    [self bindViewModel];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.scrollView.contentInset = UIEdgeInsetsZero;
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void) setupDefaults
{
    self.navigationView.layer.masksToBounds = NO;
    self.navigationView.layer.shadowOffset = CGSizeMake(0, 5);
    self.navigationView.layer.shadowOpacity = 1.0;
    self.navigationView.layer.shadowRadius = 3;
    self.navigationView.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.26].CGColor;
    
    self.addButton.layer.cornerRadius = 4;
    self.addButton.layer.borderWidth = 1.5;
    self.addButton.layer.borderColor = [UIColor colorWithRed:59.0/255.0 green:64.0/255.0 blue:84.0/255.0 alpha:1.0].CGColor;
    
    UIView *paddingLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 18)];
    self.usernameText.leftView = paddingLeft;
    self.usernameText.leftViewMode = UITextFieldViewModeAlways;
}

-(void) bindViewModel
{
    self.addButton.rac_command = self.viewModel.addCommand;
    self.back.rac_command = self.viewModel.backCommand;
    
    [self.addButton.rac_command.enabled subscribeNext:^(id value) {
        if ([value boolValue])
        {
           self.addButton.backgroundColor = [UIColor colorWithRed:59.0/255.0 green:64.0/255.0 blue:84.0/255.0 alpha:1.0];
        } else
        {
           self.addButton.backgroundColor = [UIColor clearColor];
        }
    }];
    
    [self.usernameText.rac_textSignal subscribeNext:^(id value) {
        [self.viewModel enterUsername:value];
    }];
    
    [self.problemText.rac_textSignal subscribeNext:^(id value) {
        [self.viewModel enterDescription:value];
    }];
}


-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - notifications handler

-(void) keyboardDidShow:(NSNotification *)notif
{
    CGFloat decardHeight = self.scrollView.frame.size.height/2;
    if ([self.usernameText isFirstResponder])
    {
        CGFloat positionLabel = CGRectGetMaxY(self.usernameText.frame);
        CGFloat heightUsernameText = self.usernameText.frame.size.height;
        CGFloat heightUsernameLabel = self.usernameLabel.frame.size.height;
        if (positionLabel > decardHeight)
        {
            [self.scrollView setContentOffset:CGPointMake(0, decardHeight-heightUsernameLabel-heightUsernameText) animated:YES];
        }
        NSLog(@"%f",positionLabel);
    }
    if ([self.problemText isFirstResponder])
    {
        CGFloat positionLabel = CGRectGetMaxY(self.problemText.frame);
        CGFloat heightDecsriptionLabel = self.descriptionLabel.frame.size.height;
        CGFloat heightDesciptionText = self.problemText.frame.size.height;
        if (decardHeight < positionLabel)
        {
            [self.scrollView setContentOffset:CGPointMake(0, decardHeight-heightDecsriptionLabel-heightDesciptionText)];
        }
    }
    
}

-(void) keyboardDidHide:(NSNotification *)notif
{
     [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - UITouch methods

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - action methods

- (IBAction)addPhoto:(id)sender {
}

- (IBAction)tapToBackground:(id)sender {
    [self.view endEditing:YES];
}

@end