//
//  User.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface User : RLMObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic) BOOL rooted;

@end