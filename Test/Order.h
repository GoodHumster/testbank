//
//  Order.h
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Order <NSObject>

@property (nonatomic) NSInteger id;
@property (nonatomic, strong) NSString *problem;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *status;
@property (nonatomic) BOOL agreedAdmin1;
@property (nonatomic) BOOL agreedAdmin2;
@property (nonatomic, strong) NSString *pictureURL;


@end