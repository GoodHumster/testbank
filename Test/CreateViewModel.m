//
//  CreateViewModel.m
//  Test
//
//  Created by ios on 02.06.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "CreateViewModel.h"
#import "ViewModelServices.h"
#import "OrderImpl.h"

@interface CreateViewModel ()

@property (nonatomic, weak) id<ViewModelServices> services;

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *description;

@end

@implementation CreateViewModel

@synthesize description = _description;

#pragma mark - init methods

-(instancetype) initWithServices:(id)services
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.services = services;
    [self initialized];
    return self;
}

-(void) initialized
{
    RACSignal *enabledSingal = [[RACSignal combineLatest:@[RACObserve(self, description),RACObserve(self, username)] reduce:^id{
        return [NSNumber numberWithBool:self.username.length > 0 && self.description.length > 0];
    }] map:^id(id value) {
        return @([value boolValue]);
    }];
    
    self.backCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [self.services popAnimated:YES];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
    self.addCommand = [[RACCommand alloc] initWithEnabled:enabledSingal signalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            OrderImpl *order = [[OrderImpl alloc] init];
            order.problem = self.description;
            order.id = [self getID];
            order.agreedAdmin2 = NO;
            order.agreedAdmin1 = NO;
            order.username = self.username;
            order.status = @"На расмотрений";
            [[self.services getRealmServices] saveObject:order];
            [self.services popAnimated:YES];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
}

#pragma mark - utilites methods

-(NSInteger) getID
{
    NSArray *allOrders = [[self.services getRealmServices] getAllObjectForClass:[OrderImpl class]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO];
    allOrders = [allOrders sortedArrayUsingDescriptors:@[sortDescriptor]];
    if (allOrders == nil)
    {
        return 1;
    }
    return [allOrders.firstObject id]+1;
}

#pragma mark - Publick API methods

-(void) enterDescription:(NSString *)text
{
    self.description = text;
}

-(void) enterUsername:(NSString *)text
{
    self.username = text;
}


@end